# ATCI works

Advanced Topics in Computational Intelligence (ATCI) - Master in Artificial Intelligence

- *ATCI-DDPG.ipynb*: Deep Deterministic Policy Gradient (DDPG) raw implementation with explanations and examples
- *ATCI-Exploration.pdf*: Review of different techniques for exploration in RL
    - Return distribution vs.
    - Noisy networks vs.
    - Curiosity-driven

## Author
- Armando Rodriguez